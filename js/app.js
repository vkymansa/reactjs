'use strict';

// Simple pure-React component so we don't have to remember
// Bootstrap's classes
var MaterialButton = React.createClass({
  render: function() {
    return (
      <MUITabs>
      <MUITabItem id="tab-default-1" label="Tab-1">
        <span>Pane-1</span>
      </MUITabItem>
      <MUITabItem id="tab-default-2" label="Tab-2">
        <span>Pane-2</span>
      </MUITabItem>
      <MUITabItem id="tab-default-3" label="Tab-3">
        <span>Pane-3</span>
      </MUITabItem>
    </MUITabs>
    );
  }
});

React.render(<MaterialButton />, document.getElementById('testApp'));
